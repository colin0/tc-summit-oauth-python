#!/usr/bin/env python3

import oauth
import requests
from urllib.parse import urlencode

oauth = oauth.OAuth()
authData = oauth.getToken()
instanceUrl = authData['instance_url']
apiEndpoint = instanceUrl + '/services/data/v46.0'

headers = {
        'Accept': 'application/json',
        'Authorization': oauth.getAuthHeader(),
        }
queryUrl = f'{apiEndpoint}/query?{urlencode({"q": "SELECT Id, Name FROM Account"})}'
res = requests.get(queryUrl, headers = headers)
if res.status_code != requests.codes.ok:
    raise Exception(f'unexpected status code from query callout: {res.status_code}')

result = res.json()
print('Accounts')
print('========')
for rec in result['records']:
    print(f'{rec["Id"]} - {rec["attributes"]["url"]} - {rec["Name"]}')
