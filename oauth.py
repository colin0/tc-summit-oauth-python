import webbrowser
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlencode, parse_qsl
from _thread import start_new_thread
from time import sleep

AUTH_ENDPOINT = 'https://login.salesforce.com/services/oauth2/authorize'

CLIENT_ID = '3MVG9vtcvGoeH2bjKw86OHn6zqKeDCdW35DRYdP8ENWWfOY5bBBNTbPaQgCR9UM4tqGvgoC1y9wsMGnLaSNCn'
LISTEN_PORT = 8080
REDIRECT_URI = f'http://localhost:{LISTEN_PORT}/callback'

class OauthHandler(BaseHTTPRequestHandler):
    def set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_HEAD(self):
        self.set_headers()

    def do_GET(self):
        self.set_headers()
        isCallback = self.path == '/callback'
        fn = 'oauth.html' if isCallback else 'success.html'
        with open(fn, 'rb') as f:
            self.wfile.write(f.read())
        if not isCallback:
            self.shutdown()

    def shutdown(self):
        def killServer(server):
            sleep(0.5)
            server.shutdown()
        start_new_thread(killServer, (self.server,))

    def do_POST(self):
        # Doesn't do anything with posted data
        global _authData
        length = int(self.headers['Content-Length'])
        _authData = { k: v for k, v in parse_qsl(self.rfile.read(length).decode('utf-8')) }
        if ('state' not in _authData) or ('tc-summit-demo' != _authData['state']):
            raise Exception('invalid state')
        self.set_headers()

class OAuth:
    def __init__(self):
        self.authData = None

    def getToken(self):
        if self.authData == None or self.getAccessToken() == None:
            self.redirectUserToAuthServer()
            self.runServer()
            self.authData = _authData
            print('server shutdown completed')
        return self.authData

    def getAuthHeader(self):
        return f'Bearer {self.getAccessToken()}'

    def redirectUserToAuthServer(self):
        params = {
                'response_type': 'token',
                'client_id': CLIENT_ID,
                'state': 'tc-summit-demo',
                'redirect_uri': REDIRECT_URI
                }
        url = f'{AUTH_ENDPOINT}?{urlencode(params)}'
        print('redirecting to ' + url)
        webbrowser.open(url)

    def getAccessToken(self):
        return self.authData['access_token'] if self.authData and 'access_token' in self.authData else None

    def getRefreshToken(self):
        return self.authData['refresh_token']

    def runServer(self, server_class = HTTPServer, handler_class = OauthHandler):
        port = LISTEN_PORT
        server_address = ('', port)
        httpd = server_class(server_address, handler_class)
        httpd.serve_forever()
        httpd.server_close()

